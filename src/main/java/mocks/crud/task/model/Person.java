package mocks.crud.task.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Person {

    private Long id;

    private String name;

    private Integer age;

    private Long addressId;

    private List<Long> relatives;

    public Person(Long id, String name, Integer age, Long addressId) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.addressId = addressId;
        relatives = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Long> getRelatives() {
        return relatives;
    }

    public void addRelatives(List<Long> relatives) {
        this.relatives = relatives;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(id, person.id) &&
                Objects.equals(name, person.name) &&
                Objects.equals(age, person.age) &&
                Objects.equals(addressId, person.addressId) &&
                Objects.equals(relatives, person.relatives);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, age, addressId, relatives);
    }
}
