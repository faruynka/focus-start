package mocks.crud.task.service;

import mocks.crud.task.model.Address;
import mocks.crud.task.model.Person;
import mocks.crud.task.repository.AdvancedRepository;
import mocks.crud.task.repository.CrudRepository;

import java.util.ArrayList;
import java.util.List;

public class PersonService implements AdvancedRepository {

    private CrudRepository<Long, Address> addressService;

    private CrudRepository<Long, Person> personRepository;

    public PersonService(CrudRepository<Long, Address> addressService, CrudRepository<Long, Person> personRepository) {
        this.addressService = addressService;
        this.personRepository = personRepository;
    }

    @Override
    public List<Person> findAllRelativesId(Long personId) {
        List<Long> relativesId =  personRepository.findById(personId).getRelatives();
        List<Person> relatives = new ArrayList<>();
        for (Long relativeId: relativesId) {
            relatives.add(personRepository.findById(relativeId));
        }
        return relatives;
    }

    @Override
    public Address getAddress(Long personId) {
        Person person = personRepository.findById(personId);
        return addressService.findById(person.getAddressId());
    }

    public void save(Person element) {
        personRepository.save(element);
    }

    public Person findById(Long id) {
        return personRepository.findById(id);
    }

    public List<Person> findAll() {
        return personRepository.findAll();
    }

    public Person update(Person element) {
        return personRepository.update(element);
    }

    public void delete(Person element) {
        personRepository.delete(element);
    }
}
