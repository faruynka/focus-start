package mocks.crud.task.service;

import mocks.crud.task.model.Address;
import mocks.crud.task.repository.CrudRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddressService implements CrudRepository<Long, Address> {
    private Map<Long, Address> addressRepository = new HashMap<>();

    @Override
    public void save(Address element) {
        addressRepository.put(element.getId(), element);
    }

    @Override
    public Address findById(Long id) {
        return addressRepository.get(id);
    }

    @Override
    public List<Address> findAll() {
        return new ArrayList<Address>(addressRepository.values());
    }

    @Override
    public Address update(Address element) {
        return addressRepository.replace(element.getId(), element);
    }

    @Override
    public void delete(Address element) {
        addressRepository.remove(element.getId());
    }
}
