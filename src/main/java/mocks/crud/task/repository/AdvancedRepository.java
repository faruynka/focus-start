package mocks.crud.task.repository;

import mocks.crud.task.model.Address;
import mocks.crud.task.model.Person;

import java.util.List;

/**
 * Дополнительный репозиторий
 */
public interface AdvancedRepository {
    /**
     * Метод для поиска родственников
     * @return список родственников
     */
    List<Person> findAllRelativesId(Long personId);

    /**
     * Метод для поиска адреса пользователя
     * @return список родственников
     */
    Address getAddress(Long personId);
}
