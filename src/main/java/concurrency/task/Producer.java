package concurrency.task;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

public class Producer implements Runnable {
    private String inputPath;
    private Semaphore consumerStop;
    private Semaphore bufferLock;
    private Semaphore producerStop;
    private LinkedList<String> buffer;
    private AtomicBoolean isProducerFinished;

    Producer(LinkedList<String> buffer, String inputPath, Semaphore consumerStop, Semaphore bufferLock,
             Semaphore producerStop, AtomicBoolean isProducerFinished) {
        this.inputPath = inputPath;
        this.buffer = buffer;
        this.consumerStop = consumerStop;
        this.bufferLock = bufferLock;
        this.producerStop = producerStop;
        this.isProducerFinished = isProducerFinished;
    }

    public void produce() throws InterruptedException, FileNotFoundException {
        try (Scanner scan = new Scanner(new File(inputPath))) {
            while (scan.hasNext()) {
                producerStop.acquire(1);
                String line = scan.next();
                bufferLock.acquire();
                buffer.add(line);
                bufferLock.release();
                consumerStop.release(1);
            }
        }
        isProducerFinished.set(true);
        consumerStop.release(1);
    }

    @Override
    public void run() {
        try {
            produce();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}

