package concurrency.task;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

public class Consumer implements Runnable {
    private String outputPath;
    private Semaphore consumerStop;
    private Semaphore bufferLock;
    private Semaphore producerStop;
    private LinkedList<String> buffer;
    private AtomicBoolean isProducerFinished;

    public Consumer(LinkedList<String> buffer, String outputPath, Semaphore consumerStop, Semaphore bufferLock,
                    Semaphore producerStop, AtomicBoolean isProducerFinished) {
        this.outputPath = outputPath;
        this.buffer = buffer;
        this.consumerStop = consumerStop;
        this.bufferLock = bufferLock;
        this.producerStop = producerStop;
        this.isProducerFinished = isProducerFinished;
    }

    public void consume() {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(outputPath), StandardCharsets.UTF_8)) {
            for (; ; ) {
                consumerStop.acquire(1);
                if (isProducerFinished.get() && buffer.isEmpty()) {
                    break;
                }
                bufferLock.acquire();
                String line = buffer.removeFirst();
                bufferLock.release();
                writer.write(String.valueOf(line));
                writer.newLine();
                producerStop.release(1);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        consume();
    }
}

