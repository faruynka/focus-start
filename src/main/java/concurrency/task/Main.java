package concurrency.task;

import java.util.LinkedList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

public class Main {
    private static int MAX_BUFFER_SIZE = 5;
    private static final String INPUT_PATH = "input.txt";
    private static final String OUTPUT_PATH = "output.txt";
    private static LinkedList buffer = new LinkedList();
    private static Semaphore bufferLock = new Semaphore(1);
    private static Semaphore consumerStop = new Semaphore(0);
    private static Semaphore producerStop = new Semaphore(MAX_BUFFER_SIZE);
    private static AtomicBoolean producerFinished = new AtomicBoolean();

    public static void main(String[] args) {
        producerFinished.set(false);
        Runnable producer = new Producer(buffer, INPUT_PATH, consumerStop, bufferLock, producerStop, producerFinished);
        Runnable consumer = new Consumer(buffer, OUTPUT_PATH, consumerStop, bufferLock, producerStop, producerFinished);
        Thread producerThread = new Thread(producer);
        Thread consumerThread = new Thread(consumer);
        producerThread.start();
        consumerThread.start();
    }
}