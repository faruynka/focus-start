package mocks.crud.task.service;

import mocks.crud.task.model.Address;
import org.junit.Before;
import org.junit.Test;

import static mocks.crud.task.service.TestUtils.*;
import static org.junit.Assert.*;

public class AddressServiceTest {
    private AddressService addressService;

    @Before
    public void init() {
        addressService = new AddressService();
    }

    @Test
    public void saveAndFindByIdTest() {
        addressService.save(ADDRESS_MOSCOW);
        addressService.save(ADDRESS_SAINT_PETERSBURG);
        assertEquals(ADDRESS_SAINT_PETERSBURG, addressService.findById(1L));
        assertEquals(ADDRESS_MOSCOW, addressService.findById(2L));
    }

    @Test
    public void saveAndFindByIdWrongTest() {
        addressService.save(ADDRESS_MOSCOW);
        assertNotEquals(ADDRESS_MOSCOW, addressService.findById(1L));
    }

    @Test
    public void findAllTest() {
        addressService.save(ADDRESS_SAINT_PETERSBURG);
        addressService.save(ADDRESS_MOSCOW);
        assertEquals(ALL_ADDRESSES, addressService.findAll());
    }

    @Test
    public void findAllWrongTest() {
        addressService.save(ADDRESS_MOSCOW);
        assertNotEquals(ALL_ADDRESSES, addressService.findAll());
    }

    @Test
    public void updateTest() {
        Address addressMoscow = new Address(2L, "Moskow");
        addressService.save(ADDRESS_MOSCOW);
        assertEquals(addressMoscow, addressService.update(ADDRESS_MOSCOW));
    }

    @Test
    public void updateWrongTest() {
        Address addressMoscowModified = new Address(1L, "Moskow1");
        addressService.save(ADDRESS_MOSCOW);
        assertNotEquals(addressMoscowModified, addressService.update(ADDRESS_MOSCOW));
    }

    @Test
    public void deleteTest() {
        addressService.save(ADDRESS_MOSCOW);
        addressService.save(ADDRESS_SAINT_PETERSBURG);
        assertEquals(ADDRESS_SAINT_PETERSBURG, addressService.findById(1L));
        addressService.delete(ADDRESS_SAINT_PETERSBURG);
        assertNull(addressService.findById(1L));
    }
}