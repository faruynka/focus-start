package mocks.crud.task.service;

import mocks.crud.task.model.Address;
import mocks.crud.task.model.Person;

import java.util.ArrayList;
import java.util.List;

public class TestUtils {
    public static final Person PERSON_VASYA;
    public static final Person PERSON_ALISA;
    public static final Person PERSON_MARINA;
    public static final Person PERSON_MISHA;
    public static final Address ADDRESS_SAINT_PETERSBURG;
    public static final Address ADDRESS_MOSCOW;
    public static final List<Person> VASYA_RELATIVES = new ArrayList<Person>();
    public static final List<Long> VASYA_RELATIVES_ID = new ArrayList<Long>();
    public static final List<Address> ALL_ADDRESSES = new ArrayList<Address>();

    static {
        ADDRESS_SAINT_PETERSBURG = new Address(1L, "Saint Petersburg");
        ADDRESS_MOSCOW = new Address(2L, "Moskow");
        PERSON_VASYA = new Person(1L,"Vasya", 36, 1L);
        PERSON_ALISA = new Person(4L,"Vasya", 36, 1L);
        PERSON_MARINA = new Person(2L,"Marina", 12, 1L);
        PERSON_MISHA = new Person(3L, "Misha", 18,2L);
        VASYA_RELATIVES.add(PERSON_ALISA);
        VASYA_RELATIVES.add(PERSON_MARINA);
        VASYA_RELATIVES_ID.add(4L);
        VASYA_RELATIVES_ID.add(2l);
        PERSON_VASYA.addRelatives(VASYA_RELATIVES_ID);
        ALL_ADDRESSES.add(ADDRESS_SAINT_PETERSBURG);
        ALL_ADDRESSES.add(ADDRESS_MOSCOW);
    }
}
