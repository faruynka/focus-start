package mocks.crud.task.service;

import mocks.crud.task.model.Address;
import mocks.crud.task.model.Person;
import mocks.crud.task.repository.CrudRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static mocks.crud.task.service.TestUtils.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PersonServiceTest {
    @Mock
    private CrudRepository<Long, Address> addressRepository;
    @Mock
    private CrudRepository<Long, Person> personRepository;

    private PersonService personService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        personService = new PersonService(addressRepository, personRepository);
    }

    @Test
    public void findAllRelativesTest() {
        when(personRepository.findById(1L)).thenReturn(PERSON_VASYA);
        when(personRepository.findById(2L)).thenReturn(PERSON_MARINA);
        when(personRepository.findById(4L)).thenReturn(PERSON_ALISA);
        assertEquals(VASYA_RELATIVES, personService.findAllRelativesId(1L));
        verify(personRepository, times(1)).findById(1L);
        verify(personRepository, times(1)).findById(2L);
        verify(personRepository, times(1)).findById(4L);
    }

    @Test
    public void findAllRelativesWrongTest() {
        when(personRepository.findById(1L)).thenReturn(PERSON_VASYA);
        when(personRepository.findById(2L)).thenReturn(PERSON_MARINA);
        when(personRepository.findById(4L)).thenReturn(PERSON_ALISA);
        assertNotEquals(VASYA_RELATIVES_ID, personService.findAllRelativesId(1L));
        verify(personRepository, times(1)).findById(1L);
        verify(personRepository, times(1)).findById(2L);
        verify(personRepository, times(1)).findById(4L);
    }

    @Test
    public void getAddressTest() {
        when(personRepository.findById(1L)).thenReturn(PERSON_VASYA);
        when(addressRepository.findById(1L)).thenReturn(ADDRESS_SAINT_PETERSBURG);
        assertEquals(ADDRESS_SAINT_PETERSBURG, personService.getAddress(1L));
        verify(personRepository, times(1)).findById(1L);
        verify(addressRepository, times(1)).findById(1L);
    }

    @Test
    public void getWrongAddressTest() {
        when(personRepository.findById(3L)).thenReturn(PERSON_MISHA);
        when(addressRepository.findById(2L)).thenReturn(ADDRESS_MOSCOW);
        assertNotEquals(ADDRESS_SAINT_PETERSBURG, personService.getAddress(3L));
        verify(personRepository, times(1)).findById(3L);
        verify(addressRepository, times(1)).findById(2L);
    }

    @Test
    public void saveTest() {
        doNothing().when(personRepository).save(PERSON_MISHA);
        personService.save(PERSON_MISHA);
        verify(personRepository, times(1)).save(PERSON_MISHA);
    }

    @Test
    public void findByIdTest() {
        when(personRepository.findById(1L)).thenReturn(PERSON_VASYA);
        assertEquals(PERSON_VASYA, personService.findById(1L));
        verify(personRepository, times(1)).findById(1L);
    }

    @Test
    public void findByIdWrongTest() {
        when(personRepository.findById(1L)).thenReturn(PERSON_VASYA);
        assertNotEquals(PERSON_MARINA, personService.findById(1L));
        verify(personRepository, times(1)).findById(1L);
    }

    @Test
    public void findAllTest() {
        when(personRepository.findAll()).thenReturn(VASYA_RELATIVES);
        assertEquals(VASYA_RELATIVES, personService.findAll());
        verify(personRepository, times(1)).findAll();
    }

    @Test
    public void findAllWrongTest() {
        when(personRepository.findAll()).thenReturn(null);
        assertNull(personService.findAll());
        verify(personRepository, times(1)).findAll();
    }

    @Test
    public void updateTest() {
        Person vasyaModified = new Person(1L, "Vasya", 34, 1L);
        when(personRepository.update(vasyaModified)).thenReturn(vasyaModified);
        assertEquals(vasyaModified, personService.update(vasyaModified));
        verify(personRepository, times(1)).update(vasyaModified);
    }

    @Test
    public void updateWrongTest() {
        Person vasyaModified = new Person(1L, "Vasya", 34, 1L);
        when(personRepository.update(vasyaModified)).thenReturn(vasyaModified);
        assertNotEquals(PERSON_VASYA, personService.update(vasyaModified));
        verify(personRepository, times(1)).update(vasyaModified);
    }

    @Test
    public void deleteTest() {
        doNothing().when(personRepository).delete(PERSON_MISHA);
        personService.delete(PERSON_MISHA);
        verify(personRepository, times(1)).delete(PERSON_MISHA);
    }
}